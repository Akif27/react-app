import './App.css';
import { Route, Switch, Redirect } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route
          exact
          path={`${process.env.PUBLIC_URL}/home`}
          component={() => (
            <h2> Home </h2>
          )}
        />
        <Route
          exact
          path={`${process.env.PUBLIC_URL}/contact`}
          component={() => (
            <h2> Contact </h2>
          )}
        />
        <Route
          exact
          path={`${process.env.PUBLIC_URL}/about`}
          component={() => (
            <h2> About </h2>
          )}
        />
        <Redirect
          from={process.env.PUBLIC_URL}
          to={`${process.env.PUBLIC_URL}/home`}
        />
      </Switch>
    </div>
  );
}

export default App;
